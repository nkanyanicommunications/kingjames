﻿const config = require('./config.json');
const mysql = require('mysql');

var connection = mysql.createConnection({
  host: config.host, 
  user: config.user,
  password: config.password,
  multipleStatements: true
});

module.exports = {
  saveToDatabase: saveToDatabase
};

function validateEntries(data) {
  if (data.username === undefined || data.username === "") {
    throw new Error("please enter your name.");
  }
  if (data.firstname === undefined || data.firstname === "") {
    throw new Error("please enter your surname.");
  }
  if (data.lastname === undefined || data.lastname === "") {
    throw new Error("please enter your id number.");
  }
  if (data.email === undefined || data.email === "") {
    throw new Error("please enter your id number.");
  }
}

function saveToDatabase(data, connection) {
  try {

    validateEntries(data);
    let usersParamsInsert = {
      username: data.username,
      firstname: data.firstname,
      lastname: data.lastname,
      email: data.email
    };

    return new Promise((resolve, reject) => {
      try {
        connection.connect(function (err) {
          if (err) throw err;

          usersParamsInsert.date_created = new Date().toISOString();
          connection.query(`INSERT INTO scp_qa.users SET ?`, usersParamsInsert, function (err, result) {
            if (err) throw err;

            resolve(result);
            console.log("1 record inserted");
          });
        });
        connection.end();
      } catch (error) {
        reject(error)
      };
    });

  } catch (error) {
    console.log(error);
    return Promise.reject(error);
  }
};
