﻿const config = require('./config.json');
const mysql = require('mysql');

var connection = mysql.createConnection({
  host: config.host,
  user: config.user,
  password: config.password,
  multipleStatements: true
});

module.exports = {
  saveToDatabase: saveToDatabase
};

function validateEntries(data) {
  if (data.username === undefined || data.username === "") {
    throw new Error("please enter your name.");
  }
  if (data.firstname === undefined || data.firstname === "") {
    throw new Error("please enter your surname.");
  }
  if (data.lastname === undefined || data.lastname === "") {
    throw new Error("please enter your lastname.");
  }
  if (data.email === undefined || data.email === "") {
    throw new Error("please enter your email.");
  }
  if (data.password === undefined || data.password === "") {
    throw new Error("please enter your password.");
  }
}

function saveToDatabase(data, connection) {
  try {

    validateEntries(data);
    let usersParamsInsert = {
      username: data.username,
      firstname: data.firstname,
      lastname: data.lastname,
      email: data.email,
      password: data.password
    };

    return new Promise((resolve, reject) => {
      try {
        connection.connect(function (err) {
          if (err) throw err;

          usersParamsInsert.date_created = new Date().toISOString();
          connection.query("SELECT * FROM db_test.users where username = '" + data.username + "' and email = '" + data.email + "'", function (err, result) {
            if (err) throw err;

            if (result.length > 0) {
              console.log("username or email already exist");
            }
            else {
              connection.query(`INSERT INTO db_test.users SET ?`, usersParamsInsert, function (err, result) {
                if (err) throw err;
                connection.end();
                resolve(result);
                console.log("record successfully added");
              });
            }

          });
        });
      } catch (error) {
        reject(error)
      };
    });

  } catch (error) {
    console.log(error);
    return Promise.reject(error);
  }
};
